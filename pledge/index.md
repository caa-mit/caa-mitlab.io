---
layout: blog.pug
title: Scientists Against Apartheid Pledge
date: 2023-12-04T17:00:00-04:00
subtext: We call on all students and workers to withhold their labor from companies and research projects that are complicit in Israeli apartheid and the occupation, ethnic cleansing, and genocide of the Palestinian people.
---

[Sign here](#sign-below) and read [the signatories list](#signatories).

The scientific research and labor of the students and workers at MIT have far too long been used by companies and the military to prop up the cruel apartheid regime of Israel and its brutal occupation of Palestine. Our technology and knowledge, which we create to benefit humankind, have been twisted for this purpose. We must stand against the complicity of our institution and fight for the full liberation of Palestine until the occupation falls.

We draw inspiration from the historic international boycott and divestment movement that fought against the system of South African apartheid, a movement which thus inspired the Palestinian-led [BDS](https://bdsmovement.net/call) movement through conversations with Palestinian and South African activists in the 2000s. We know that MIT, as a leading global technological institution, serves as a major appendage of U.S. imperial interests, which in turn wholly sustains the Israeli occupation forces. Notably, there are at least 8 MIT professors who have received direct research funding from Israel's Ministry of Defense in the last 5 years.[^1] It is thus imperative for us to actively fight against the Israeli occupation, particularly within MIT – the heart of the US war machine.

**We call on all students and workers to withhold their labor from companies and research projects that are complicit in Israeli apartheid and the occupation, ethnic cleansing, and genocide of the Palestinian people.** There will be no more science for genocide, no more technology for apartheid, and no more research for occupation. We will not work for or with any entity that is complicit in these crimes against humanity until they fully boycott and divest from Israeli apartheid.

## <u>Stand with Palestine, No Jobs for the Israeli War Machine.</u>
**I pledge** to neither pursue nor accept any positions (internship, part-time job, full-time job, etc.) at the companies in the list below that are complicit in Israeli apartheid, occupation, and the violation of Palestinian rights.\
**I pledge** to not attend recruiting events or any other career-related events that feature or are funded by these companies.\
**I pledge** to refuse to devote my labor to companies beyond this list with ties to the Israeli occupation.

**These companies include:**
- [Lockheed Martin](https://www.lockheedmartin.com/en-il/who-we-are.html)
- [HP](https://nebula.wsimg.com/978e38f1e98e33b82746924901dde810?AccessKeyId=7BB3DAA86ABC0388A877&disposition=0&alloworigin=1)
- [Boeing](https://www.seattletimes.com/business/boeing-sped-delivery-of-1000-bombs-to-israel/)
- [Dell](https://www.whoprofits.org/companies/company/7370?dell-technologies)
- [Project Nimbus (a Google and Amazon project)](https://www.theguardian.com/commentisfree/2021/oct/12/google-amazon-workers-condemn-project-nimbus-israeli-military-contract) 

This list of companies was created with the understanding that as scientists and engineers at MIT, we have an obligation to withhold our labor from the atrocities committed by the Israeli occupation against the Palestinians. We demand these companies end their culpability in Israel’s crimes by boycotting and divesting from Israel until the occupation falls. 

## <u>Research for the People, Not Genocide.</u>

As a researcher at MIT, I understand that advances in science and engineering can be tools of apartheid, whether it is directly through the development of military technology or indirectly by benefiting corporations that have ties to the Israeli occupation. We believe that boycotting research with ties to Israeli occupation is an exercise of academic freedom and an act of solidarity with the Palestinians, whose universities have been bombed, schools have been closed, and scholars and students murdered or displaced .

**I pledge** to refuse to accept funding from the listed companies (Lockheed Martin, HP, Boeing, Dell, Project Nimbus) that are complicit in Israeli apartheid and occupation\
**I pledge** to refuse research projects that advance military interests of the occupation (e.g. weapons, surveillance)

_It is through collective action that we see change, making each student’s current or future employment and research decisions drastically impactful. Any type of contribution to the companies listed below and/or research projects, even if not straightforward, makes us complicit by association. By pledging to abstain from companies and research projects supporting the Israeli occupation, we apply the public pressure needed to declare that the scientists, engineers, students, and workers of MIT will not engage in work that supports the genocide and occupation of the Palestinian people._

[^1]: Information about sponsored funding can be found [here](https://vpf.mit.edu/about-vpf/publications) under the Report of Sponsored Research Activity (aka the Brown Book). 

## Sign below:

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfrMWb0CK7pc6DXhjdBz8jkiNMhtNa3CQ3Sj8xj-TWq5NK1iQ/viewform?embedded=true" frameborder="0" marginheight="0" marginwidth="0" style="width:auto; height: 30em; border: 5px solid black">Loading…</iframe>

## Signatories:

{% for person in signatories %}
- {{ person }}
{% endfor %}
