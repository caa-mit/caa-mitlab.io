const string = require('string');
const slugify = s => string(s).slugify().toString();

module.exports = function (eleventyConfig) {
    eleventyConfig.addPassthroughCopy("**/*.png");
    eleventyConfig.addPassthroughCopy("**/*.jpg");
    eleventyConfig.addPassthroughCopy("**/*.svg");
    eleventyConfig.addPassthroughCopy("**/*.pdf");
    eleventyConfig.addPassthroughCopy("style.css");
    eleventyConfig.amendLibrary("md", mdLib => {
        mdLib.use(require('markdown-it-footnote'));
        mdLib.use(require('markdown-it-anchor'), { slugify });
        mdLib.renderer.rules.footnote_block_open = () => (
            '<div><hr></div><h2 class="mt-3">Sources:</h4>' +
            '<section class="footnotes">' +
            '<ol class="footnotes-list">'
        );
    });
    eleventyConfig.addGlobalData("signatories", async () => {
        const req = await fetch('https://docs.google.com/spreadsheets/d/e/2PACX-1vRU5zo2gpXVyKrdeFjZeDF_Bqtf8A0KEZsaezwFMyDm0vZv4Zby32VZadmie5S2ZcvG84Or2KtYg87x/pub?gid=460125674&single=true&output=tsv');
        const text = await req.text();
        return text.split('\n').filter((v, i, a) => !i || v != a[i - 1]);
    });
};
