---
layout: blog.pug
title: 'Statement from the MIT Arab Student Organization'
date: 2023-11-15T9:15:00-04:00
---

<blockquote style="direction: rtl">
    <p>
        ”اّه يا جرحي المكابر، وطني ليس حقيبة. وأنا لست مسافر, إنني العاشق ...والأرض حبيبة“
        <strong>
            ~محمود درويش
        </strong>
    </p>
</blockquote>

> “Ah my intractable wound! My country is not a suitcase. I am not a traveler. I am the lover and the land is the beloved.” **~Mahmoud Darwish**

The MIT Arab Student Organization (ASO) is in unwavering solidarity with the people of Palestine who have been enduring immense suffering as a result of the Israeli occupation. The atrocities being committed in Gaza, where more than 11,000 Palestinians have been killed, are nothing short of war crimes and a humanitarian catastrophe. We vehemently oppose this genocide and fully support students’ rights to peacefully protest against it.

We strongly condemn the attempts by the MIT administration to restrict the fundamental rights of free speech and peaceful assembly. During the peaceful protest on November 9th, the administration's threats of suspension were used as tools to stifle our community’s voices. Despite this aggression, students continued to exercise our right to peaceful protest, and we commend the resilience of our members. The right to free expression and peaceful protest are fundamental rights that should never be compromised.

Despite a rise in anti-Arab racism across the US and here at MIT, the administration has not been taking effective steps with regards to discrimination towards our constituents. It has come across that racism towards some is more acceptable to the institute than racism towards others. There should not be an “initial focus” on supporting only one group; our community members feel seen as secondary.  The deep fear that the Institute’s silence on racism has instilled in the hearts of our Arab community members has led to fear even identifying as Arab on campus or speaking out about that fear.

Recent actions by the MIT administration, such as the newly updated policies regarding protesting, facilitate the censorship of our community and undermine our rights. We stand in solidarity with all student groups at MIT and beyond who have been threatened by the suspension of campus activities. This unjust and heavy-handed approach to handling protests and demonstrations is detrimental to the spirit of free expression and open discourse on campus.

We also emphasize the importance of being part of the movement for Palestine on campus. It is a matter of conscience and standing up against oppression. As a diverse and vibrant community, we have a responsibility to support our Palestinian members who have been directly affected by this violence and advocate for an end to the ongoing crisis. The struggle for justice and human rights is interconnected, and we recognize that our community's fight for justice goes hand in hand with the struggle for justice in Palestine as part of the wider struggle for the liberation of all oppressed peoples. Palestinians are an integral part of our community, and we are committed to supporting their resistance and demand for liberation from the Israeli occupation and apartheid.

The MIT Arab Student Organization stands firm in our commitment to justice, humanity, and dignity for all. We will continue to work tirelessly to ensure that the voices of our community members are heard and that the rights of free speech and peaceful assembly are upheld without compromise. Like many other student groups on campus, we are committed to this cause and want to make it clear that an attack against one of us is an attack against us all. We urge the MIT administration to reconsider their stance and prioritize the values of justice, equality, and human rights.

Until liberation,\
MIT Arab Student Organization
