---
layout: blog.pug
title: 'Latin America and Israel with Prof. Les Field'
date: 2023-11-15T12:00:00-04:00
tags: teachin
---


<iframe class="container" style="aspect-ratio:560/315" src="https://www.youtube-nocookie.com/embed/FkuCu1-R_nE?si=zn3HtpSrC1apAYyk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Field will be touching on some of the history of Israel's involvement in Latin America, with a focus on Colombia, Argentina, and Central America in the 1980s.