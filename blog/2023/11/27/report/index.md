---
layout: blog.pug
title: 'MIT Administration Ignores the Pattern of Harassment and Safety Concerns of Students Critical of Israel, Deprioritized Fighting Islamophobia at MIT, Violated MIT’s Freedom of Expression Policies by Suppressing Peaceful Protests'
date: 2023-11-27T12:00:00-04:00
tags: posts
---

**Send questions / comments to: [eyesonmit@gmail.com](mailto:eyesonmit@gmail.com)**

_Beginning on October 7th, the world has watched horrific events unfold in an unprecedented and violent 52 days of incalculable suffering, loss and preventable misery. The MIT campus and community— with its diverse composition-- has been affected greatly. As a result, tensions on campus have been high as students, faculty and staff alike experience personal losses, mourn, engage in political demonstrations and express solidarity._

_While MIT prides itself on fostering an inclusive and diverse learning environment in which students from around the world can engage in the highest level of education, become global leaders and work to solve the world’s toughest problems, MIT has ignored a pattern of behavior perpetuated by pro-Israel community members against those who are critical of this foreign government and the start and expansion of yet another war. From Muslim to Jewish to Arab, Black, and Queer students, repeated incidents with numerous witnesses and some even on camera have not yet warranted a sufficient reaction from MIT._

_Rather, MIT has deprioritized the fight against islamophobia on campus — leaving Muslim, Arab, and Palestinian students subject to continued public harassment, doxxing, and even physical assault._

_Further, despite MIT’s history and policies on Freedom of Expression, the current MIT administration has acted in an unprecedented manner to suppress protests and demonstrations and suspend students for engaging in protests in historic protest locations recently made prohibited._

_At such a critical and historic moment for American and global history, the decision to limit peaceful demonstrations and free speech on campus, while ignoring the pattern of bigotry and targeted harassment on campus leaves valuable members of the MIT community alone and isolated in the face of a genocide in Palestine, per [the UN](https://www.ohchr.org/en/press-releases/2023/11/gaza-un-experts-call-international-community-prevent-genocide-against) and [genocide scholars](https://jewishcurrents.org/a-textbook-case-of-genocide)._

_The events of the Lobby 7 Protest and Counterprotest exemplify this great ongoing challenge that the MIT community is facing, and have shined a light on this issue that must be dealt with, not forgotten, in order for MIT to continue its legacy as a global, leading academic institution._

– **Members of the MIT Community**

* * *

## Lobby 7 Protest: November 9th

On November 9, various anti-war student groups at MIT planned a peaceful demonstration for global solidarity with Palestine to call for an end to violence and civilian casualties. Despite last-minute restrictions on protest locations by the MIT administration, dozens of students proceeded with their sit-in at Lobby 7, a traditional site for such demonstrations. During the demonstration, the anti-war protesters created and maintained safe pathways through Lobby 7 to permit any passing student, community member or guest to safely pass by and get to class if needed.

**This peaceful demonstration was met with aggressive disruption from counter-protesters, leading to physical and verbal assaults on the demonstrators, who endured derogatory slurs and threats.** The situation escalated as these counter-protesters, rejecting the orderly passages through the sit-in, resorted to force and intimidation, including displaying graphic posters and projected footage to disrupt the protest further. This account is corroborated by MIT Faculty Members who were there, and others who witnessed the same aggression at other demonstrations on campus by the same group of pro-Israel counter-protesters.

In a move that seemed to undermine the sanctity of academic freedom and peaceful assembly, the MIT administration issued a threat of suspension to any student continuing to protest via letter, casting a shadow of censorship and authoritarianism over the institution. The protest, however, adapted and continued, with students peacefully sitting, studying, and socializing, even as the administration locked down the building, restricting access and movement.

**At the end of the night, the MIT administration announced a suspension for only the anti-war protesters on the basis of their remaining in Lobby 7 despite requests to leave– the aggressive and violent behavior of the counter-protesters was unacknowledged and those students did not face suspension.** The disregard for academic freedom of speech continued the next day, with the cancellation of a scheduled “teach in” educational event the next day, where students were barred from entering a classroom (10-250) by police, further stifling the voice of the student body.

In the events unfolding the next few days, MIT announced that the Committee on Discipline (COD) would evaluate the cases since the protest was a violation of MIT policy. **In response, [MIT Faculty Newsletter released a statement](https://fnl.mit.edu/mit-values-and-the-protests-in-lobby-7/) highlighting that the administration’s actions goes against MIT’s own policies, and discriminates against the protests and protesters.**

**MIT’s public statements so far have chosen to conceal the actions and identity of the organization to which the counter-protesters belong; failing to call out their aggressive, hateful and even violent actions has created a campus environment in which anyone who is anti-war or critical of the Israeli government feels unsafe** — including but not limited to students of diverse identities such as Muslim, Jewish, Arab, Black, and Queer students. The repeated communications from MIT reiterate only the perspective of the counter-protesters that they are “fearful”, yet several of these students were perpetrators of targeted hate speech and physical aggression. Also in attendance were counter-protesters who remained peaceful and MIT faculty sympathetic to the counter-protesters were present in Lobby 7. Neither party acted to subdue the hostile behavior of those who acted inappropriately.

**After the events of November 9, the MIT Administration has stated that minimizing antisemitism on campus is a more timely and important priority than fighting other forms of bigotry such as Islamophobia.** With the public evidence that has been documented, this blatant institutional bias makes campus unsafe for those whose experiences have been ignored, downplayed and sidelined: Muslims, Jewish students who support a ceasefire, queer students, women and Black students—as detailed below.

In response, a cohort of student organizations **has released statements of support and solidarity with the targeted and collectively punished protesters:** [MIT Jews for Ceasefire](https://docs.google.com/document/d/e/2PACX-1vROyk6X2v7LWeVqY0yBxL8j0eogt5vftTTpFSQDtRb3v9OrJOYGWWbTDUCOFXwqC0IY3nulaHoMGuuR/pub), [MIT Coalition for Palestine](https://docs.google.com/document/d/1-PIUZ64aHbcnEtBE7TsgMI9tKmUYWYvg5y8r4G-Q8DA/edit), [MIT Coalition Against Apartheid](https://caa.mit.edu/blog/2023/11/12/statement/), Palestine @ MIT, [Black Graduate Student Association of MIT](https://bgsa.mit.edu/blog/2023/11/14/bgsa-statement-in-solidarity-with-the-mit-for-palestine-coalition), [MIT Black Students’ Union](https://twitter.com/still_francesca/status/1724625008998191545), [MIT Asian American Initiative](https://twitter.com/still_francesca/status/1724564436239663363), [MIT Arab Student Organization](https://twitter.com/still_francesca/status/1724884871921717562), MIT Divest, [MIT Reading for Revolution](https://justpaste.it/920vf), MIT Student Workers Alliance, [MIT alumni](https://docs.google.com/document/d/1qeF7wafpkusadW4oY26Tv4D5lXXLdV9BNjoGkexscqw/edit) and [MIT Alumni for Free Speech](https://www.mitalumforfreespeech.com/).

**A cohort of students who attended the protest put together [a timeline of events here](https://docs.google.com/document/d/1iG4hmi_Vr0utcJOvWA4kxdGjazfqxY0YIzkoLoFZjpo/edit?usp=sharing); this timeline has been cross-referenced with video footage and matches the statement published in the MIT Faculty Newsletter.**

Please find resources below outlining these events.

## An Unaddressed Pattern of Harassment and Discrimination on MIT Campus

### November 9th Lobby Protest Incidents

MIT PhD student [Francesca Riccio Ackerman](https://twitter.com/still_francesca/status/1722668619320992088) has begun a long twitter thread beginning on Nov 9th that shows evidence of physical aggression, harassment and abusive language by the counter-protestors, as well as many other follow-up events by the MIT Administration and the MIT community.

-   Pro-Israel counter-protesters physically assaulted students, stole and threw anti-war students’ posters and reached across police barricades to enact more physical violence. → [Shown here](https://x.com/still_francesca/status/1722833045131055173?s=20) (please watch the entire video for the last incident).
-   Anti-war Jewish and Israeli students experienced many antisemitic comments and hate. They have since mobilized to create the Jews for Ceasefire group & released statements denouncing the MIT Israel Alliance. → [Described here.](https://x.com/still_francesca/status/1723179788787961992?s=20)
-   Women (hijabi) students were targeted with sexually explicit protest sign messages shoved in their faces & accused of supporting rape → [Seen here.](https://x.com/still_francesca/status/1723062627331461450?s=20)
-   Queer students were singled out and told that they would be killed in Gaza - felt attacked and uncomfortable with this verbal abuse. → [Seen here.](https://x.com/still_francesca/status/1725209440704491550?s=20)
-   Black students were falsely accused of touching/harming pro-Israel students and had cops called on them. → [Described here.](https://x.com/still_francesca/status/1722754618734649420?s=20)
-   Arab-appearing students were targeted and screamed at “you look like a terrorist”. → [Shown here.](https://x.com/still_francesca/status/1723076960643871221?s=20)

**[Video Clips of Harassment and Discrimination During Nov 9th Protests](https://drive.google.com/drive/folders/15PlvYHC_tSZ-BWuFWmeo4Ccig1y12n9M)**
-   Pro-Israel counter-protester calls a bearded man a terrorist
-   Pro-Israel counterprotestor snatches anti-war protestors from peaceful protestors
-   Pro-Israel protestor physically assaulting anti-war protestors
-   Pro-Israel counter protestor reaches through the police barricade to shove a Muslim anti-war student

### A Pattern of Harassments: Pre-November 9th Incidents

**Members of the MIT Israel Alliance, other identified pro-Israel counterprotesters and other people who are intolerant of criticism of Israel have been recorded prior to November 9th engaging in dozens of incidents such as:**

-   Repeated anti-Arab, anti-Palestinian and Islamophobic racism
-   Repeated antisemitic abuse for any Jewish person who does not support Israel’s actions
-   Telling students to “go back to their country” or “go back to Palestine”
-   Calling students who criticize the Israeli government “backwards”
-   Telling members of their own dorm and study groups that they are no longer welcome for expressing concern about the ongoing bombing campaign in Gaza
-   Telling students “Fuck you for supporting terrorism” for sharing MIT event flyers online
-   Ostracizing students from friend groups and publicly humiliating them in dining halls
-   Chasing hijabi students while publicly yelling derogatory slurs about Islam and Muslims
-   Former members of the IDF threatening to “come after” every other MIT student who criticizes Israel
-   Repeatedly publicly accusing students of supporting terrorism and rape because of their criticism of Israel
-   TAs and Professors being seen photographing/videotaping their students to threaten academic/professional retaliation for engaging in protests
-   Repeatedly accusing students of antisemitism for criticisms of Israel and the Israeli government
-   Publicly targeting and humiliating queer-presenting students for their criticism of Israel and making violent comments about what would happen to them for being queer in Palestine

#### Student Testimonies from Before November 9

>“I am worried about many things including being attacked and harassed for being Arab and Muslim. I am worried about my professional career in the US. I am worried about being able to express my political opinions peacefully after what happened at Harvard. I am worried about my family and friends back home. I am worried about visa issues. I am worried about growing anti-Arab sentiment and every time I enter to through the borders.”

>“I am worried \[about\] being called a "terrorist" just because of the way I look, my religion, the language I speak and political views. I am worried ... I hope MIT hears my concerns and worries, which I cannot express vocally in fear of being attacked by racist groups, business owners and government organizations.”

>“There was no safe space on campus today for Palestinian, Arab, and Muslim students. There was nowhere for us to go but to our homes instead of going to our labs and classes.”

>“Campus is not safe right now and that's not okay. Verbal harassment would probably be the least of my worries. I'm worried for my physical safety. I've heard enough raw anger from the Israeli rally today that makes me not want to find out.”

>“They were talking to me about how morally "backwards" Hamas, Palestinians, Iran, and "other Muslim countries" were in how they treated women and queer people and asked if I didn't prefer "progressive" countries like Israel and the US. I told them I'm not from here so that is not an argument that would land with me and they told me if I don't like it then I should go back to my home country then.”

>“\[I\] received a message on Instagram from an MIT student "Fuck you for supporting terrorism and kidnapping kids and elderly disabled women and the killing of my literal school friends."--- the post in question did not support terrorism in any way.”

>“I was texted and asked to stay away from my friends in my dorm, who were also my pset group because of my posts about Palestine.”

#### Publicly Targeting and Threatening Students across Campus for Criticism of Israel

For instance, [here is a video](https://x.com/still_francesca/status/1725209440704491550?s=20) published by a member of the MIT Israel Alliance from before November 9, in which she chases down and video-records a student who took down an unauthorized poster with the intent of humiliating and doxxing her. The face of the victim was blurred by Francesca, not the student who targeted her. The student writes, **“I’m going to expose each and every one of the supporters of these barbaric \[sic\] animals, rapists, aka Hamas.”**  Another MIT student responded with a homophobic comment: **“this person doesn’t realize if they ever said ‘did you just assume my gender’ in Gaza they would be shot on the spot”**.

## Responses by the Faculty and MIT Community

### Letters by Faculty, Staff and Alumni

#### [November 15th-- MIT Faculty Newsletter Denouncing MIT’s Response](https://fnl.mit.edu/mit-values-and-the-protests-in-lobby-7/)

>“Having spoken to participants, witnesses, marshals, and faculty observers about the demonstration in Lobby 7 on Thursday, November 9, there is compelling evidence that the protesters were engaged in a peaceful demonstration of their values.”

>“However, there was a disruption in the morning when counter-protesters entered the space and became extremely provocative, shouting and violating the personal space of some of the protesters.”

>“Nevertheless, the administration has decided unilaterally to punish the protesters. We find this deeply problematic, especially given MIT’s values – not just the value of free speech, but also the commitment to fostering integrity and well-being.”

#### [November 14th-- MIT Faculty & Staff Open Letter on Freedom of Expression and Student Safety at MIT](https://docs.google.com/document/d/18zj4igUMzPPMTy7LuejxMbB34jPqi12mRvMjN-WJ7Wg/edit)

Signed by over hundred MIT’s senior faculty, staff and concerned individuals, and sent on November 14th.

>“We have watched with disappointment and unease over the past five weeks as the Institute has reacted disproportionately to student activists, as it stifled the voices of our student community members….We remind the MIT administration per the Faculty's Statement on Freedom of Expression that the Institute values ‘civility, mutual respect, and uninhibited, wide-open debate,’ and ‘even robust disagreements shall not be liable to official censure or disciplinary action.’”

>“It is crucial for our institute to honor the diverse experiences of our students, staff, and faculty, and to equip our future leaders with the ability to navigate a global community. Honoring those experiences requires every member of our community to be empowered to speak out on issues of import, without fear of retribution that may endanger not only their academic careers but their visa status.”

#### [MIT Alumni for Free Speech Statement](https://www.mitalumforfreespeech.com/)

Signed by over 500, the letter shows commitment to withhold support of MIT until the “Administration issues a public apology for the hasty and harmful response to Lobby 7 sit-in”.

>“"We morally balk at a Palestine-exception to free speech, and we call on MIT to strive to create an environment where all community members can voice their concerns."

The letter also highlights how the protesters were in full spirit of MIT community’s values:

>“Civil disobedience is a vital component of the vibrant culture of MIT. We are a community famous for getting a police car on the Dome; a community who, in the 1960-70s, [](https://radius.mit.edu/programs/power-protest-mit-march-4th-50-years)[protested en-masse to demand immediate withdrawal from Vietnam](https://radius.mit.edu/programs/power-protest-mit-march-4th-50-years) and successfully lobbied MIT to [](https://web.mit.edu/fnl/volume/314/king_bernstein.html)[divest from the DOD-sponsored Instrumentation Lab](https://web.mit.edu/fnl/volume/314/king_bernstein.html); who, in the 1980-90s, [](https://www.blackhistory.mit.edu/archive/shantytown-built-protest-coalition-against-apartheid-1987)[built a shantytown in front of the Student Center](https://www.blackhistory.mit.edu/archive/shantytown-built-protest-coalition-against-apartheid-1987) to draw awareness to [](https://www.thecrimson.com/article/1986/3/15/shantytown-defenders-arrested-at-mit-peight/)[MIT Corporation’s investments in the apartheid regime in South Africa](https://www.thecrimson.com/article/1986/3/15/shantytown-defenders-arrested-at-mit-peight/);  who, in recent years, [](https://grist.org/climate-energy/david-kochs-impact-on-the-longest-fossil-fuel-sit-in-ever/)[held a 116 day long sit-in](https://grist.org/climate-energy/david-kochs-impact-on-the-longest-fossil-fuel-sit-in-ever/) in the Infinite to demand that the university cut fossil fuel holdings from its endowment. MIT has this rich history of activism precisely because of the people MIT seeks to attract - people who want not only to lead the advancement of science and technology, but also to apply these tools to create a [](https://www.mit.edu/building-a-better-world/)[better world](https://www.mit.edu/building-a-better-world/).”

#### [MIT Alumni Denouncing Punishment and Silencing of Students Protesting for Palestine](https://docs.google.com/document/d/1qeF7wafpkusadW4oY26Tv4D5lXXLdV9BNjoGkexscqw/edit)

Sent to the MIT President by hundreds of alumni over 10-12th of November, the letter condemns the response of the MIT administration:

>“We are alarmed by videos from the counter-protest that arose, of counter-protestors shoving MIT Police and students, slamming away signs of people sitting together, and calling the peaceful protestors, “terrorists”. The responsibility for these incitements was incorrectly placed equally on both sides in the latest MIT statement to the community.”

### Statements by MIT Student Organizations

#### [November 9th -- MIT-Wide Coalition for Palestine Press Release](https://docs.google.com/document/d/1OjVB5jbdvEr7xiU-TYvXInakp5zmrITM1wKQ7pVNNVw/edit)

>“The MIT-wide Coalition for Palestine, representing multiple cultural and political student organizations, is currently holding a peaceful demonstration in MIT Lobby 7 to demand that MIT stop funding research that supports Israeli apartheid and the ongoing genocide against the Palestinian people. These protests have only expanded despite absurd and retaliatory threats by the MIT administration against dozens of students for peacefully protesting.”

>“The MIT-wide Coalition for Palestine stands strong against these fear tactics. We condemn this action by the administration for exactly what it is, a means of suppressing our voices”

#### [November 13th -- Statement by MIT Coalition for Palestine](https://docs.google.com/document/d/1-PIUZ64aHbcnEtBE7TsgMI9tKmUYWYvg5y8r4G-Q8DA/edit)

>“The MIT Coalition for Palestine is primarily focused on opposing the widespread oppression and genocide faced by the Palestinian people. We strongly denounce the misinterpretation and distortion of our actions, as well as the dissemination of misinformation on actions which transpired this past week. We firmly stand against the MIT administration's unjust censorship and discrimination against the Coalition. It is deeply concerning that an educational institution, particularly one that prides itself on upholding free speech, would hinder students from hosting an educational event and engaging in a peaceful protest. This represents a clear effort to stifle our voices. While they may threaten to suspend individual students, they cannot suspend our movement.”

#### [November 10th– Jewish and Israeli Students Issue Statement Condemning MIT Israel Alliance and its members’ actions during the November 9th Protest](https://docs.google.com/document/u/6/d/e/2PACX-1vR4b5F__co5dPPeamruQ_c03uD5JVeCumX7JJUR0_lNLlWgjBX2OICh4ioh1QyHGaAPZf_gjcsERq-L/pub)

>“We, the Jewish and Israeli students involved in the Palestinian Liberation protest of November 9th, are writing this in response to a widely circulated statement that claims to speak for all Jews/Israelis at MIT. Said statement attempts to justify genocidal rhetoric and action by the Israeli and U.S. governments against Palestinians in the name of Jewish safety. We unequivocally refute and condemn this false equivalence, especially in light of counter-protestors’ actions against MIT Coalition Against Apartheid (CAA) organizers and supporters. We absolutely disavow and are insulted by the statement MIT Israel Alliance made. False claims of anti-Jewish behavior only inflame antisemitism and make it more difficult to hold nuanced conversations on a very real bigotry.  As Jewish participants at Thursday's event, we were only made to feel unsafe by the actions of counter-protestors. We had to withstand inflammatory and hateful remarks like “G\_d made a mistake by having you born a Jew,” and were called “antisemitic,” “self-hating Jews,” and “Hamas.” We find the conduct of the counter-protesters shameful and infuriating.”

#### [November 14th-- Open Letter to MIT Administration by MIT Jews for Ceasefire](https://docs.google.com/document/d/e/2PACX-1vROyk6X2v7LWeVqY0yBxL8j0eogt5vftTTpFSQDtRb3v9OrJOYGWWbTDUCOFXwqC0IY3nulaHoMGuuR/pub)

>“We, the Jewish and Israeli students involved in the Palestinian Liberation protest of November 9th, are writing this in response to a widely circulated statement that claims to speak for all Jews/Israelis at MIT. Said statement attempts to justify genocidal rhetoric and action by the Israeli and U.S. governments against Palestinians in the name of Jewish safety. We unequivocally refute and condemn this false equivalence, especially in light of counter-protestors' actions against MIT Coalition Against Apartheid (CAA) organizers and supporters.”

#### [November 14th-- Statement by Black Graduate Student Association of MIT](https://bgsa.mit.edu/blog/2023/11/14/bgsa-statement-in-solidarity-with-the-mit-for-palestine-coalition)

>"We stand in solidarity, not just as a group or a community, but as a collective force that yearns for change, for justice, for freedom. We, The Black Graduate Student Association, reaffirm and stand unequivocally with the MIT CAA in their leadership of the MIT Coalition for Palestine, and we fully support these organizing efforts."

#### [November 14th-- Statement by MIT Asian American Initiative](https://twitter.com/still_francesca/status/1724564436239663363)

>“We, the members of the MIT Asian American Initiative, affirm our commitment to the liberation of Palestine and the Palestinian people's right to resist over 75 years of violent occupation by the settler-colony of Israel. We defend the right of every human to live a life of dignity. We affirm our solidarity because we have a responsibility to stand on the side of justice for all oppressed peoples.”

#### [November 14th-- Statement by MIT Black Students Union](https://twitter.com/still_francesca/status/1724625008998191545)

>“Our fight for equity and global liberation is one and the same as the fight to Free Palestine, and the MIT Black Students’ Union will continue to fight until we have succeeded.”

#### [November 15th-- Statement by MIT Arab Student Organization](https://twitter.com/still_francesca/status/1724884871921717562)

>"We strongly condemn the attempts by the MIT administration to restrict the fundamental rights of free speech and peaceful assembly. We stand in solidarity with all student groups at MIT and beyond who have been threatened by the suspension of campus activities."

#### [November 15th-- Statement by MIT Reading 4 Revolution Organization](https://justpaste.it/920vf)

>"The US has, as it often does, invoked a blatant double-standard by ignoring human rights concerns in Palestine simply because of its inconvenience to US interests."

## Further Aggression by Pro-Israel Groups and Individuals on Campus

### Letter by the MIT Israeli Alliance to MIT Administration Slandering Anti-War Students as Supporters of Terrorism

**[MIT Israeli Alliance Open Letter](https://docs.google.com/document/d/1QDm22xSu9NUsptPwLzr8tFmXTlAzKKoGTkCeg4xXVDM/edit):** Notably, in this letter, despite repeated clarification by Arabic-speaking students, the group has decided the meaning of Arabic words to be violent and threatening as a fear tactic and to align other supports with anti-Arab and anti-Muslim bias against the students. Evidence presented throughout the letter show cropped screenshots with ambiguous messages that are sensationalized and intentionally removed from context that would clarify their meaning.

### Slander and Misinformation Spread by an MIT Professor

**[An MIT Professor, Retsef Levi, publicly doxxed](https://x.com/RetsefL/status/1721415263105515646?s=20)** an MIT student (released full name + photo) on Twitter to an audience of millions, tagging Bill Ackman to try to ruin the student’s academic and professional career for protesting the Lockheed-Martin MISTI Israel program for its connection to weapons manufacturing.

**[The same MIT Professor, Retsef Levi](https://x.com/RetsefL/status/1722852140245254559?s=20)** spread misinformation that has been debunked to over 7M viewers claiming that Jewish students could not pass through Lobby 7 and slandering the protesters as “justifying the terror attacks of Hamas”. This misinformation was later debunked by the MIT administration’s public statement.

### MIT Open Faculty Meeting: A Concerning Incident of Political Suppression

There was an open faculty meeting on November 15th during which a Professor (Michel DeGraff) who spoke in defense of the Palestinian people and the anti-war student protesters was literally shouted down by another MIT professor (“Daniel Blankschtein is shouting ‘ENOUGH, ENOUGH, SHUT UP, SHUT UP’” per attendees notes), and others made inappropriate remarks in the public Zoom chat. If the students facing suppression raises one alarm, then seeing an MIT professor being silenced by his own colleagues should raise 10. Professor DeGraff also attended at least one demonstration on campus (not Nov 9) and personally witnessed the aggression and targeted behavior of the counter-protesters even in the presence of children.

## Response by MIT Administration

**Nov 8th: [MIT’s Official letter to the community barring protests in Lobby 7](https://orgchart.mit.edu/letters/campus-safety-and-policies-around-protests-postering-and-free-expression)**

> “The only approved protest venues are the following outdoor spaces: Stratton Student Center, the steps of the Stata Center’s Dertouzous Amphitheater, Kresge Oval, McDermott Court, and Hockfield Court. These spaces must be reserved in advance with reasonable notice. Group requests to use sound amplification will be considered.”

**Nov 9th: [MIT’s Official letter to the Community on Nov 9th](http://inj9.mjt.lu/nl3/5guo3-ydhytxrZHgwY-Dvw?m=AXAAAC0GLx8AAcrtEZAAAJNTcwUAAAAAGqoAJdFPAAiQzwBlTaSBcqM0VtCmQgq26yMhr0_lyQAIIWc&b=4b67c4b4&e=ef470308&x=XTAMnqzBLpnVhOtxoG4ljQ)**

>“I want to describe some events that occurred at MIT today and reflect briefly on what they mean for our community. As many of you know, starting at 8:00 this morning, Lobby 7 and the opening of the Infinite Corridor were the site of significant protest and eventually counterprotest. **I am deliberately not specifying the viewpoints**, as the issue at hand is not the substance of the views but where and how they were expressed.”

**This is a first, and was interpreted by students who were harassed and assaulted as a deliberate decision to conceal the identities of the pro-Israel counterprotesters and their organization.**

**Nov 14th: [MIT’s Official Update to Campus Tensions on Nov 14th](https://orgchart.mit.edu/letters/update-campus-tensions)**

>“In response to the tragic violence in the Middle East, a protest and counterprotest in Lobby 7 last Thursday have stirred considerable distress, confusion, conflicting claims and false rumors, on campus and well beyond.”

**Nov 14th: [MIT’s Official FAQ on Response to Events on Nov 14th](https://president.mit.edu/updates/update-events-november-9)**

>“Although antisemitism will be the initial focus, the charge will be broader, and will include efforts to address prejudice and hate against Arabs and Muslims, as well.”

**Nov 16th: [MIT Open Faculty Meeting Notes - Will Prioritize Antisemitism over Other Forms of Hate](https://twitter.com/still_francesca/status/1725196292341584147?s=46)**

>This was stated to faculty pushing back against this bias in an open meeting. The full statement is detailed below.

### MIT President states that antisemitism will be handled first, Islamophobia later

During the November 15th Open Faculty meeting, **MIT President Sally Kornbluth made the following statement, clarifying her decision to prioritize antisemitism over Islamophobia at MIT:**

>“I’ve condemned what’s going on in Gaza as well, I think it’s awful that we are seeing the suffering of innocent children, of innocent people—we are people! So my comments about starting with antisemitism first are very specific to our situation here, in the United States, and I’m going to tell you why: Jews represent 2.5% of the population and represent 55% of recorded religious and national hate crimes right now—and so, look—I didn’t say we’re never going to get to Islamophobia, I’m trying to harness a tired community of faculty that have other things to do. **I wanted to mobilize an effort on antisemitism quickly, and I wasn’t talking about an effort on Islamophobia 2 years from now, I was talking a MONTH from now so that people didn’t feel that, you know, we’re giving a half-hearted effort to one thing or another-  that’s the first thing.** The second thing I’ll say—and you know, I have to give credit to \[Redacted\] for opening my eyes to this a little bit where he went.. I have received probably 10:1 the number of very, very concerning—and today, one so shocking I lost my breath—of antisemitic events that are going on. People from outside saying things that you would not believe, okay? I believe that there are many Islamophobic incidents that have not risen to my attention and \[Redacted\] has said, and I think it’s probably true, that they’re underreported, because MIT and the United States in general is viewed as more friendly to the Jewish/Israeli cause and maybe it’s not bubbling up in the way that it should, but I’m telling you: the things I’ve heard that are anti-semitic in the last couple of weeks…. I’m \[redacted age\] years old, and I am a Jew and I have NEVER heard things like that In my life. So THAT is why we’re starting with an effort on antisemitism. It’s not personal, I don’t mean to personalize it. I’m just telling you, if people are saying it to ME, what are they saying to our freshmen, okay?”

## MIT Administration’s Contradictory Remarks

In a [private exchange](https://drive.google.com/file/d/1AuJjIUwyigz3u09SaiAt79WlhMlHaKfU/view) on 13 November with the MIT Israel Alliance (MITIA), later shared online by the group, MIT President Sally Kornbluth said: "It's been falsely rumored that Jewish students were prevented from attending class…While we recognise a heightened sensitivity given the increase in reports of antisemitism around the country and in greater Boston, **we have received no evidence of any kind that Jewish students, or anyone else, were prevented from attending class,"** Kornbluth added.

The next day, President Kornbluth reiterated in a public [statement](https://web.archive.org/web/20231115140236/https://president.mit.edu/updates/update-events-november-9) that there was "a specific unease about passing through Lobby 7" though "**it is not accurate that movement around the MIT campus is constrained**".

That same public statement, however, was later [updated](https://president.mit.edu/updates/update-events-november-9) to **contradict President Kornbluth’s own words:** it was changed to say that "some students were impeding access to the Infinite Corridor" and that, given the loud nature of the demonstration, "it is no surprise that some students felt afraid of passing through Lobby 7".

### MIT Violation of Freedom of Expression

**[December 23, 2022 Update on MIT’s Free Expression and Academic Freedom Statement](https://facultygovernance.mit.edu/december-23-2022-update-free-expression-statement)**

MIT faculty claim that this December 2022 statement is being reversed with the administration’s recent actions in response to the November 9 Protest.

>“We cannot prohibit speech that some experience as offensive or injurious. At the same time, MIT deeply values civility, mutual respect, and uninhibited, wide-open debate. In fostering such debate, we have a responsibility to express ourselves in ways that consider the prospect of offense and injury and the risk of discouraging others from expressing their own views. This responsibility complements, and does not conflict with, the right to free expression. Even robust disagreements shall not be liable to official censure or disciplinary action. This applies broadly. For example, when MIT leaders speak on matters of public interest, whether in their own voice or in the name of MIT, this should always be understood as being open to debate by the broader MIT community.”

>“**This commitment includes the freedom to criticize and peacefully protest speakers to whom one may object, but it does not extend to suppressing or restricting such speakers from expressing their views.** Debate and deliberation of controversial ideas are hallmarks of the Institute’s educational and research missions and are essential to the pursuit of truth, knowledge, equity, and justice.”

## Developing Media Coverage of the Events

### In Middle East Eye

**[Israel-Palestine war: MIT students undeterred by looming suspension over pro-Palestine rally](https://www.middleeasteye.net/news/israel-palestine-war-facing-suspension-mit-university-students-continue-pro-palestine-advocacy)**

>“In a matter of days, a solidarity rally for Palestinians in Gaza was met with a concerted campaign to paint protesters as antisemitic.”

>“A pro-Palestine sit-in protest at the Massachusetts Institute of Technology (MIT) earlier this month has sparked a slew of accusations of antisemitism and violence from a pro-Israel group, coupled with a concerted effort to permanently ban a student group calling for a ceasefire in Gaza.

>The Coalition Against Apartheid (CAA), at the centre of organising around ending Israel's occupation of Palestine, has become a target. But despite this campaign in an already stifling climate around Palestine activism with several universities banning pro-Palestine groups, students remain undeterred to continue their advocacy.”

### In The Chronicle of Higher Education

**[At MIT, Fear, Frustration and Flailing Administrators](https://archive.is/cRSyi)**

>“According to Chen, the administration has placed blame for rising tensions on campus solely on pro-Palestinian students, despite the fact that many of their demonstrations have been peaceful…The November 9 protest at MIT, and the administration’s much-criticized response, exemplify the escalating tensions on campuses since Hamas attacked Israel on October 7. Many students have been arrested or suspended, a few colleges have banned the groups behind the demonstrations, and many administrators are caught in an angry crossfire, unsure how to move forward.”

### In The Nation

**[College Administrations Are Failing Their Palestinian and Jewish Students](https://www.thenation.com/article/activism/university-protests-antisemitism-islamophobia/)**

>“The \[Jews for Ceasefire at MIT’s\] founding was in large part instigated by witnessing a peaceful sit-in on November 9, in which members of MIT’s Coalition Against Apartheid were assaulted by counterprotesters…’The counterprotesters,’ he told me, ‘quickly became violent,’ making sexist and Islamophobic remarks, and taking pictures of women protesters. ‘I intervened by blocking the camera view with my tallit…and the counterprotesters got really mad at me, throwing awful verbal insults, saying things like, ‘God made a mistake by having me born a Jew.’’ He emphasized: ‘The only anti-Jewish remarks I experienced that day came from those counterprotesters.’”

### In Democracy Now

**[Students at MIT, Brown, Columbia Risk Arrest, Retaliation to Protest War on Gaza](https://www.democracynow.org/2023/11/10/headlines/students_at_brown_mit_columbia_and_other_colleges_risk_arrest_retaliation_to_protest_war_on_gaza)** [On the No Science for Genocide movement at MIT\]

>“\[students\] come to MIT because they care about science, they care about engineering. They want to do good for the world. But the thing is that they come to MIT and become disillusioned, because we find out that what our work is going towards is not for good, is not for people. It’s going towards war, endless war.”

### In The Boston Globe

**[MIT administrators warn pro-Palestinian student protesters, and counter-protesters, of possible suspension](https://archive.is/BEHAi%23selection-1559.0-1559.106)**

>“The MIT-wide Coalition for Palestine stands strong against these fear tactics,” organizer Alejandro Tañón Díaz said in the statement. “We condemn this action by the administration for exactly what it is, a means of suppressing our voices. We are confident that our movement will only grow.”

### In BreakThroughNews

**[MIT Threatens to Suspend Students](https://www.instagram.com/reel/CzhFUyGAzG0/?igshid=MzRlODBiNWFlZA%3D%3D)**

>“MIT Threatens to Suspend Students For Holding A Pro-Palestine Rally MIT is threatening to suspend its students for holding a rally for Palestine.”
