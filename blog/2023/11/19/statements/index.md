---
layout: blog.pug
title: 'Collection of statements by Coalition for Palestine'
date: 2023-11-19T12:00:00-04:00
tags: posts
---

1. [Joint Statement](/blog/2023/11/12/statement/)
1. [MIT Black Students’ Union](/blog/2023/11/14/bsu_statement/)
1. [MIT Reading for Revolution](/blog/2023/11/14/r4r_statement/)
1. [MIT Asian American Initiative](/blog/2023/11/14/aai_statement/)
1. [Black Graduate Student Association at MIT](/blog/2023/11/15/bgsa_statement/)
1. [MIT Arab Student Organization](/blog/2023/11/15/aso_statement/)