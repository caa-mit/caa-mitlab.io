---
layout: blog.pug
title: 'Palestine 101: Learn about the History of this Conflict, With Professor Leila Farsakh'
date: 2023-11-14T12:00:00-04:00
tags: teachin
---

<iframe class="container" style="aspect-ratio:560/315" src="https://www.youtube-nocookie.com/embed/uhAyp5wywq8?si=9jyhrRC3zfq_D_1F" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> 

Farsakh explains the history of what is happening in Palestine, Anti Zionism vs Antisemitism, and the role of student movements.