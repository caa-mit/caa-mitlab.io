---
layout: blog.pug
title: "MIT Reading for Revolution's Statement as part of the MIT Coalition for Palestine"
date: 2023-11-14T22:00:00-04:00
---

We are Reading for Revolution (R4R), a political reading group at MIT. We explore our unique responsibilities as citizens, students, and engineers and strive to create a more just and human-oriented society. Through the analysis of speeches, essays, and various works from revolutionary leaders across current and historical global liberation movements, we develop our consciences of theory and praxis for a truly equitable society—one free from imperialism, poverty, and all other forms of exploitation.

Reading for Revolution, as a part of the Coalition for Palestine, is fully committed to the complete decolonization and liberation of Palestine. We condemn in the strongest possible terms the Israeli violence against the Palestinian people and the United States’ complicity in bankrolling this massacre to the tune of nearly $4 billion dollars a year [^1]. We assert that the Israeli program against Palestine is a colonial one. The occupation of Palestine is a textbook example of the systems of oppression and dehumanization.

## On dehumanization and the dichotomization of “good and evil”

Throughout the course of this conflict, the Israeli and US governments have imposed and employed several of the most sadistic and insidious elements of the colonialist handbook. The Israeli War cabinet has repeatedly framed the Palestinian people as subhuman, calling them “human animals [^2].” Netanyahu reaffirms the dehumanization of Palestinian people by calling the war a “struggle between the children of light and the children of darkness [^3].”

It is this same binary that identified Indigenous peoples as “savages” and “brutes” that led to the colonization of the Americas, the “manifest destiny” ideology, and the genocide of Indigenous peoples in the United States. It is important to state that the displacement, oppression, and dehumanization of Indigenous peoples remain in effect today. It is this same binary that continues to dehumanize Black, Brown, and all marginalized communities in the United States. Expanding broadly beyond the United States, it is this same binary that has formed the crux of all colonialist and imperialist projects: the labeling of one group as “good,” and the other as “evil.”

## On the silencing of students from universities

All across the nation, members of Palestinian liberation groups at universities have been harassed, threatened, attacked, and arrested. Infamously, a doxxing truck at Harvard University paraded around its campus, displaying the faces and names of students in organizations that stood in solidarity with Harvard’s Students for Justice in Palestine as “Harvard’s Leading Antisemites [^4].” Recently, the same doxxing truck came to Columbia University [^5]. Columbia University additionally suspended their Students for Justice in Palestine and Jewish Voice for Peace organizations [^6]. Brandeis University has ordered the immediate dismantling of Brandeis’s own Students for Justice in Palestine organization [^7]. The University of Chicago arrested both student and faculty protestors during a peaceful sit-in [^8].

At MIT, we have witnessed this silencing in the administration’s blockading of Coalition Against Apartheid (CAA)’s peaceful protest in Lobby 7, which was subject to harassment from counter-protests and threats of suspension from MIT administration. This blockading occurred on the grounds of rules about “approved protest spaces.” However, these rules were never before enforced, even in demonstrations earlier in the semester that took place in and near Lobby 7. Last semester, students organizing to demand justice for Arif Sayed Faisal, a 20-year old Bangledeshi UMass Boston student who was shot and killed by Cambridge police near MIT campus, held rallies in Lobby 7 as well. However, these students did not experience the same administrative shut down response that the CAA experienced.

## On “America’s values of democracy, freedom, and human rights”

The US has, as it often does, invoked a blatant double-standard by ignoring human rights concerns in Palestine simply because of its inconvenience to US interests. During the ongoing invasion of Ukraine, the US government firmly invoked the right of the Ukrainian people to resist subjugation from Russia, and strongly condemned Russia’s war crimes in the region [^9]. We are witnessing in many ways a parallel conflict in Gaza between an overwhelming external colonialist force conducting war crimes on a mass scale and an oppressed people seeking to defend their homeland. However, instead of supporting the Palestinian people in the name of human rights and self-determination, the United States has sent tens of billions of taxpayer dollars to keep the machine of Israeli colonialism in operation. The message that the United States has sent is clear: human rights have become a tool of our hegemonic foreign policy, deployed whenever beneficial to us, and ignored in times of inconvenience.

The MIT administration’s attempts to strangle demonstrations for Palestinian liberation are shameful and constitute a clear attempt to stifle the voices speaking up for the Palestinian people. MIT’s stationing of police outside of room 10-250 to block CAA’s teach-in on November 10th reveals its hypocrisy: a supposed institution of higher learning banning educational events on campus. Students have the right to freedom of speech, to educate themselves, and to peacefully organize and protest. MIT has a long history of student activism. Students organized protests during the Vietnam War [^10] and, in the previous iteration of CAA, the South African apartheid regime [^11].


## On solidarity

The fight for Palestinian liberation is important within the larger struggle for the liberation of all oppressed peoples who have suffered at the hands of colonialism and imperialism, both at home and abroad. Throughout history, we have seen time and time again that oppressed peoples, under the yoke of colonialism and imperialism, will always strive for liberation. From Haiti, to Vietnam, to Algeria, to the Philippines, to Ireland, to the ongoing Indigenous Land Back movement, to the ongoing liberation of Black people from institutional systems of racism in the US, the Palestinian liberation movement belongs to these same struggles for a fundamental human right: to live freely, free from all systems of oppression. The systems of oppression that affect us at home are the exact same ones that subjugate nations abroad; thus lies the basis of our solidarity. We believe this resolutely: no one is free until everyone is free.

We again affirm that we are a part of the Coalition for Palestine. An attack on any one of our groups constitutes an attack against the entire coalition. We are committed to fighting for the right of all people to live a life of dignity and of all states to exist free from imperial and colonial rule. We are fully committed to standing up against the Israeli apartheid and genocide of the Palestinian people.

Signed,\
MIT Reading for Revolution

[^1]: [https://obamawhitehouse.archives.gov/the-press-office/2016/09/14/fact-sheet-memorandum-understanding-reached-israel](https://obamawhitehouse.archives.gov/the-press-office/2016/09/14/fact-sheet-memorandum-understanding-reached-israel)
[^2]: [https://x.com/AJEnglish/status/1711376122674303191?s=20](https://x.com/AJEnglish/status/1711376122674303191?s=20)
[^3]: [https://www.newarab.com/news/netanyahu-deletes-palestinian\-children-darkness-tweet](https://www.newarab.com/news/netanyahu-deletes-palestinian-children-darkness-tweet)
[^4]: [https://www.thecrimson.com/article/2023/10/12/doxxing-truck-students-israel-statement/](https://www.thecrimson.com/article/2023/10/12/doxxing-truck-students-israel-statement/)
[^5]: [https://president.columbia.edu/news/announcing-doxing-resource-group](https://president.columbia.edu/news/announcing-doxing-resource-group)
[^6]: [https://www.jewishvoiceforpeace.org/2023/11/10/cu-appalling/](https://www.jewishvoiceforpeace.org/2023/11/10/cu-appalling/)
[^7]: [https://www.nbcboston.com/news/local/brandeis-university-bans-pro-palestinian\-student-group/3183990/](https://www.nbcboston.com/news/local/brandeis-university-bans-pro-palestinian-student-group/3183990/)
[^8]: [https://chicagomaroon.com/40547/news/ucpd-arrests-protesters-engaged-in-admissions-office-sit-in/](https://chicagomaroon.com/40547/news/ucpd-arrests-protesters-engaged-in-admissions-office-sit-in/)
[^9]: [https://www.whitehouse.gov/briefing-room/speeches-remarks/2022/03/26/remarks-by-president-biden-on-the-united-efforts-of-the-free-world-to-support-the-people-of-ukraine/](https://www.whitehouse.gov/briefing-room/speeches-remarks/2022/03/26/remarks-by-president-biden-on-the-united-efforts-of-the-free-world-to-support-the-people-of-ukraine/)
[^10]: [https://web.mit.edu/fnl/volume/314/king\_bernstein.html](https://web.mit.edu/fnl/volume/314/king_bernstein.html)
[^11]: [https://www.blackhistory.mit.edu/archive/shantytown-built-protest-coalition-against-apartheid-1987](https://www.blackhistory.mit.edu/archive/shantytown-built-protest-coalition-against-apartheid-1987), [https://www.blackhistory.mit.edu/archive/anti-apartheid-student-rally-1986](https://www.blackhistory.mit.edu/archive/anti-apartheid-student-rally-1986)
