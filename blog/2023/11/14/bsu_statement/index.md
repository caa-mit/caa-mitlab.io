---
layout: blog.pug
title: 'MIT Black Students’ Union Statement in Support of the Coalition Against Apartheid at MIT'
date: 2023-11-14T19:00:00-04:00
---


## Overview:
- This past Thursday, students were threatened with suspension by MIT administrators for peacefully protesting the genocide of the Palestinian people.
- MIT PD was deployed to barricade the protest (of mostly Black, Brown, Arab, and Muslim students) inside Lobby 7. 
- The BSU unequivocally condemns the MIT Administration’s attempts at intimidation and silence.
- The liberation of the Palestinian people is intricately tied to the global liberation of Black people – none of us are free until we all are. 
- The BSU reaffirms our support for the [Coalition Against Apartheid’s Principles of Unity](https://www.instagram.com/p/CygjRM8Jyab/?img_index=2).

This past Thursday, November 9th, the MIT Coalition Against Apartheid (CAA) hosted a peaceful demonstration in Lobby 7 to denounce the Israeli occupation and genocide of the Palestinian people. After facing harassment, threats, and violence from counter-protestors, students received physical letters from MIT Administration stating that “any student who remains here [in Lobby 7] as a protestor after 12:15 today will be subject to suspension from MIT.” Upon request, MIT Administration refused to promptly clarify the details of this suspension (including its duration or severity), demonstrating their intent to intimidate a peaceful protest composed of mostly Black, Brown, Arab, and Muslim students.

At 5 PM that same day, MIT police officers were stationed at every point of egress in Lobby 7 for the remainder of the demonstration, regulating who could enter and exit the building. Students were physically blocked from accessing the building and re-entering Lobby 7, preventing protestors from using the restroom and getting food; notably, faculty members were allowed to enter the space at will to record the protest. On the morning of Friday, November 10th, the MIT Police Department was similarly deployed to block all student access to a teach-in hosted by the CAA in 10-250, effectively censoring an event centered on education. **The MIT Black Students’ Union completely condemns the acts of intimidation, censorship, and restriction of CAA’s peaceful protest by MIT Administration, as well as MIT Administration’s threat of suspension to silence protest participants.**

Black students at MIT are no strangers to increased police presence in the midst of fighting for liberation, and it is shameful that the MIT Administration weaponized the MIT Police Department to disproportionately suppress and intimidate students who advocate for the liberation of Palestine. On Friday, several students of color were verbally and physically harassed by counter-protestors under MIT PD supervision. As we stand for the safety and rights of Black MIT students, we also stand for the safety and rights of all students who are oppressed on the basis of their identity. In the face of this attempted suppression, **the MIT BSU unilaterally supports the CAA and the Free Palestine movement, at MIT and beyond our campus.**

Black liberation is interwoven with the struggle for Palestinian liberation – none of us are free until we all are. The violent consequences of imperialism and colonialism are evident in the current age of mass incarceration and militarized police states. We must not forget that the racialized violence which Black people have been subjected to is the same racialized violence which Palestinians are experiencing under occupation. The white supremacist system of oppression that marginalizes Black people is the same system that has stripped Palestinians of their humanity for the past 75+ years and perpetuated antisemitism against Jewish people globally. However, the creation of an apartheid nation state that abuses this very same white supremacist system of oppression is not the solution to marginalization. Our fight for equity and global liberation is one and the same as the fight to Free Palestine, and the MIT Black Students’ Union will continue to fight until we have succeeded. 

BSU is not deterred by the intimidation of the MIT Administration. We are committed to the **liberation of Palestine.** We support the **right of oppressed and occupied people to resist their oppression.** We fight for Palestinian liberation within the broader movement for the **liberation of all oppressed peoples.** We defend the right of every human to **lead a life of dignity.**

In solidarity,\
MIT Black Students’ Union 