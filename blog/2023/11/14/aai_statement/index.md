---
layout: blog.pug
title: "MIT Asian American Initiative statement on Palestine"
date: 2023-11-14T16:30:00-04:00
---

The events of the Thursday, November 9th sit-in led by the MIT Coalition Against Apartheid and the MIT administration’s subsequent threats to suspend involved students and student organizations make it clear that **MIT Asian American Initiative’s concrete and visible commitment to the liberation of Palestine is urgent and necessary**.

**We, the members of the MIT Asian American Initiative, affirm our commitment to the liberation of Palestine and the Palestinian people’s right to resist over 75 years of violent occupation by the settler-colony of Israel. We defend the right of every human to live a life of dignity. We affirm our solidarity because we have a responsibility to stand on the side of justice for all oppressed peoples.**

The MIT administration’s response to the peaceful demonstration on November 9th disproportionately oppresses students of color, including Asian students. For one, it should not be forgotten that Muslim, Jewish, Black, and Brown Asian Americans are experiencing compounding harm at this time. For two, the threat of student suspension and implied visa revocation suppresses Asian international students and their ability to advocate against injustice. For three, increased police presence endangers Black and Brown members of our community. Finally, the threat of suspending student organizations is a threat to silence our voices through fear and division. MIT’s interim decision to retract some of these threats after sustained student and alumni efforts still leaves students and organizations in limbo.

As a pan-Asian advocacy organization, we believe it is important to connect the struggle for Palestinian liberation to Asian American liberation. As Asian Americans with different cultures, citizenship status, language knowledge, class positions, disability status, religion, and connection to our heritage, there is much to divide us. But we must challenge this sense of division. In the United States, Asian people are triangulated against Black, Latine, Arab, and Indigenous people and beneath white people as a model minority. We are expected to remain grateful for this position, “earned” by our subservience to the white supremacist order, and we are urged to remain fearful of losing it. These tactics drive a wedge between Asian Americans and other communities, erasing our common histories and oppressions in order to divide and control us. But Asian liberation will not come from our conditional proximity to whiteness.

Many members of our community are intimately familiar with the enduring trauma of colonialism, both within Asia and by Western powers. In all cases, the colonizers’ forced removal of people from their ancestral lands has upended lives and killed countless individuals and families. As a group with colonialism woven deep into our histories and as current settlers on Indigenous land, we have a duty to act as we see this same trauma occur in Palestine at the hands of Israel and the United States.

In times of crisis, it becomes clearer than ever that Asian American complicity—“keeping our heads down”—cannot be the means to freedom and change. We, as Asian Americans, must forge our own liberation through solidarity with all minoritized identities. We must cast aside our fears, our tendencies toward self-sufficiency, and fill the gaps in our knowledge of each others’ histories. There is beauty in being in community, in learning from the struggle for liberation of all oppressed peoples everywhere, and in coming together to co-create the alternative futures we deserve.

**The MIT Asian American Initiative stands united as part of the Coalition for Palestine–an attack on one of us is an attack on all of us.** Alongside other organizations in the Coalition, we demand no science for genocide and the end to MIT's complicity in supporting the settler-colony of Israel.

This statement can also be viewed [here](https://docs.google.com/document/d/19XtcDUMHKzyLnX03IqJzbxxcv8PHgqA_U5CmukgHeqw/edit?usp=sharing).

Signed,\
MIT Asian American Initiative
