---
layout: blog.pug
title: MIT Coalition for Palestine Statement on the Events of 11/9 and 11/10
date: 2023-11-12T12:00:00-04:00
eleventyComputed:
  thumbnail: "{{ page.url }}demonstration.jpg"
tags: posts
---

## Background

The MIT Coalition for Palestine, composed of the MIT Coalition Against Apartheid (CAA) and various student cultural and political organizations, organized a peaceful demonstration scheduled for November 9 from 8:00AM to 8:00PM as part of the [global shutdown for Palestine](https://www.shutitdown4palestine.org/). However, on Wednesday, November 8, less than 24 hours before the planned demonstration, the MIT administration issued new guidelines regarding approved locations for and forms of protest. Notably, these guidelines excluded Lobby 7, the designated location for our planned demonstration, despite this location having often been a site for past demonstrations.

## Beginning of Demonstration

We carried out our planned peaceful demonstration, where students would sit around MIT’s main lobby with many students also fasting for the duration of the demonstration. Appointed shift leads and marshals actively maintained open egress and ingress for people to pass through, ensuring the demonstration remained peaceful. During the first hour of our demonstration, participants engaged in activities such as studying and playing music.

## Counter-protesters initiate disruption of demonstration

At approximately 9:00AM, counter-protesters disrupted the demonstration, physically and verbally assaulting protesters. Counter-protesters initiated this disruption by forcefully shoving their way through marshals and protesters–ignoring requests for them to use the designated paths for ingress and egress–and instead claimed they were being intentionally blocked. As such, many protesters who remained seated were physically injured by counter-protesters.

At that time, aggression from counter-protesters escalated. Several individuals, including police, who attempted to deescalate the situation were forcefully shoved to the ground by counter-protesters. Protesters' signs and belongings were forcefully taken and subsequently damaged by the counter-protesters. Many female protesters faced verbal threats, with counter-protesters telling them they would "get raped by Hamas." Protesters were labeled as terrorists, and counter-protesters chanted, "Palestinians have got to go." Additionally, counter-protesters set up a projector to play graphic footage onto the pillars of Lobby 7. This is not a comprehensive list of all the incidents which occurred.

A thread with video proof of many of these incidents can be found [here](https://twitter.com/still_francesca/status/1722668619320992088?s=46&t=2eyXHTqoR-2VNC6b6mQB1g).

The protesters then pivoted to a picket line and chanted in response to the assault.

## Suspension Threat

Around 11:45AM, less than half of the students were handed a letter stating that “any student who remains here \[Lobby 7 and the Infinite\] as a protestor \[sic\] after 12:15 today will be subject to suspension from MIT.” Due to the abrupt nature of the delivery and the unprofessional tone in the letter, many protesters disregarded it, questioning its legitimacy.

By 1:00 PM, the counter-protesters had evacuated, and the situation settled. Consequently, the demonstration shifted back into a sit-in. Students continued to lead chants, and an organizer gave a brief speech. Students then peacefully sat, studied, and socialized in Lobby 7, resuming the original plan of a peaceful and quiet demonstration, with more students joining as the afternoon progressed.

Additionally, students and community members from the Boston area heard about our demonstration and MIT’s threat of suspension, and were motivated to join the sit-in. During this time, we continued to have marshals and shift leads who worked to actively maintain ingress and egress. MIT Campus Police also remained present in Lobby 7 during this period.

## Lockdown

At 4:55 PM, MIT issued an "MIT advisory" distributed to the MIT community, including undergraduates, graduates, faculty, and staff. The advisory stated: "Please avoid Lobby 7 due to an ongoing demonstration. Seek alternate routes. Beginning at 5 PM, MIT Main Group buildings will be accessible via MIT ID card only."

Subsequently, there was a significant increase in police presence in Lobby 7, with an officer stationed at every entrance and outside, preventing entry for anyone. We were not provided advance notice of this lockdown, resulting in some protesters being denied re-entry. Exiting participants were not permitted to return, with no exceptions made for obtaining food, using the bathroom, or preparing for prayer. Faculty members were allowed to enter and exit as needed.

After the closure of Lobby 7, student supporters came into the building through non-blocked doors and went to the 2nd floor balcony overlooking Lobby 7. After campus police were stationed at the doors of the 2nd floor balcony, students began to filter into the 3rd floor balcony. Since people were not permitted to enter Lobby 7, the students on the 2nd and 3rd floors provided food and supplies to the demonstrators on the 1st floor with makeshift pulleys.

Protesters continued to socialize, do their homework, play music, and dance during this time. Around 6:30PM, protesters who were not allowed to join inside, including MIT community members and allies at other schools, held a small rally right outside the Lobby 7 doors in support of the demonstrators inside. Around 7:15PM, people were allowed to use the restroom and return but were required to have a faculty escort with them. Students who were also participating in the fast broke their fast at 8:00PM.

## Interaction with MIT Administration

While the demonstration continued, the steering committee for the MIT Coalition for Palestine drafted and released a short [press statement](https://docs.google.com/document/d/1OjVB5jbdvEr7xiU-TYvXInakp5zmrITM1wKQ7pVNNVw/edit), which was shared with multiple media contacts. Additionally, we engaged in multiple phone calls and meetings with members of MIT administration to push them to retract their threat of academic suspension for peaceful student protesters.

Eventually, with prolonged pressure from the demonstration and meetings with organizers, the administration adjusted their language and reduced their threat to a suspension from “non-academic campus activities,” which was communicated to the MIT community in a [statement](http://inj9.mjt.lu/nl3/5guo3-ydhytxrZHgwY-Dvw?m=AWgAAC3ZnEEAAcrtEZAAAMm0l4AAAAAAGqoAJdFPAAiQzwBlTaS4AtNjcWQ7RSyOCYbT7u2AKQAIIWc&b=38571c0c&e=af103bb3&x=wZ9_b-MF8db8_VYx5l3jDQ) by President Sally Kornbluth around 10:30PM after the conclusion of the demonstration.

Around 8:40PM, Lobby 7 began to reopen. The demonstration lasted over 12 hours, ending after 9:00PM. MIT administration had still refused to publicly retract their original threat of individual academic suspension despite pressure from students and the broader MIT community. At the conclusion of the demonstration, participants cleaned up the space and left for the night.

## Teach-in Cancellation

The following day, the MIT Coalition Against Apartheid (CAA) had scheduled a day of teach-ins to build community on campus and educate attendees about Palestine. This event was meant to feature professors, student organizers, and community members from all around the nation and was meant to take place in room 10-250. The CAA had reserved this space ahead of time and was approved access to by all the proper channels.

The morning of the event, three police officers were stationed at the entrance of the room and were barring entry, telling students that the event was canceled. The MIT administration said this event was a violation of the suspension from non-academic campus activities that they had issued less than 24 hours before.

## Moving Forward

The MIT Coalition for Palestine is primarily focused on opposing the widespread oppression and genocide faced by the Palestinian people. We strongly denounce the misinterpretation and distortion of our actions, as well as the dissemination of misinformation on actions which transpired this past week. We firmly stand against the MIT administration's unjust censorship and discrimination against the Coalition.

It is deeply concerning that an educational institution, particularly one that prides itself on upholding free speech, would hinder students from hosting an educational event and engaging in a peaceful protest. This represents a clear effort to stifle our voices. While they may threaten to suspend individual students, they cannot suspend our movement.

MIT's attempts to shut us down only serve to build our strength. We are a united force on a campus, national, and global scale.

On November 17, we will participate in furthering our solidarity for the Palestinian people as part of the International Day of Solidarity with Student Struggle.

As a diverse and broad Coalition, we remain steadfast and committed to the liberation of Palestine. As such, an attack on one of us, is an attack on all of us.

Signed,

MIT Coalition for Palestine
- MIT Coalition Against Apartheid
- Palestine @ MIT
- MIT Black Students Union
- MIT Black Graduate Students Association
- MIT Asian American Initiative
- MIT Arab Student Organization
- MIT Divest
- MIT Reading for Revolution
- MIT Student Workers Alliance