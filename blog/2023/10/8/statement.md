---
layout: blog.pug
title: Joint Statement on the Current Situation in Palestine
date: 2023-10-08T12:00:00-04:00
tags: posts
---

Palestine@MIT and the MIT Coalition Against Apartheid hold the Israeli regime responsible for all unfolding violence. We unequivocally denounce the Israeli occupation, its racist apartheid system, and its military rule. Colonization is inherently violent, aimed at erasing and replacing indigenous peoples. We affirm the right of all occupied peoples to resist oppression and colonization.

Israel cut Gaza off from the rest of the world for two decades, trapping over 2 million Palestinians inside the largest open-air prison. The blockade has caused severe shortages of life-saving medicines, food, electricity, and clean water, making life unbearable. Ever since the blockade, everyday life had been mired with never ending humanitarian crises, violence, and bans on the most basic human rights. 

What is happening in occupied Palestine is a response to an ongoing 75 years of occupation. It comes as a response to the settler colonial regime and the systemic oppression against Palestinians. It is a response to the constant murder of Palestinians in the West Bank, massacres in Gaza, pogroms, illegal annexation of Palestinian territories, and the millions of Palestinian refugees who are denied the right to return. The root of the current situation is this decades-long oppression.

Any media narrative that does not acknowledge the occupation and the siege as the root cause is insidious and inaccurate. 

We must keep sharing, educating, and protesting in solidarity in order to pressure our government to end their support for the Israeli regime. We are committed to supporting decolonization efforts in Palestine, and we recognize our role to oppose Zionism from within the imperial core. For more information about Palestinian resistance against Israeli occupation, check out some resources here.

**Until liberation,**
- MIT Coalition Against Apartheid
- Palestine@MIT
- Brandeis Students for Justice in Palestine
- Wellesley Students for Justice in Palestine
- Boston University Students for Justice in Palestine
- MIT Black Graduate Student Association
- MIT Reading for Revolution
- MIT National Organization of Minority Architects Students
- All Souls Movement

