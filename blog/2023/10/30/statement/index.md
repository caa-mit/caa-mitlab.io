---
layout: blog.pug
title: MIT, we demand no science for genocide!
date: 2023-10-30T12:00:00-04:00
tags: posts
---

The Coalition Against Apartheid condemns the ongoing genocide in the Gaza Strip and MIT’s complicity through the MISTI-Israel Lockheed Martin Seed Fund. Our ties to the genocidal Israeli occupation forces corrupt our scientific and moral integrity. As
scientists and engineers, we strive for a better world for all, not a continuation of humanity’s worst crimes. Our work must uphold the highest standards of morality, not directly implicate us in genocide. [^1][^2][^3] We must actively oppose oppression, not stay silent and complicit as Palestinians are slaughtered before our eyes.

In our long campaign to dismantle the ties to genocide and occupation that tarnish our institute, **<ins>our first call is to end the MISTI-Israel Lockheed Martin Seed Fund</ins>**.

In 2019, MISTI-Israel established a relationship with Lockheed Martin to create a seed fund [^4] that allows MIT professors to work directly with faculty in Israeli institutions to develop research projects and work within Lockheed Martin facilities in the US and Israel. [^5][^6] The fund chooses proposals based upon their fit within “Lockheed Martin’s Global Strategy”, [^7] which in Israel is focused on the areas of “superior support for the Israeli Defense Forces” and “ongoing industrial collaboration” with Israeli companies in defense, technology, venture capital, and research and development. [^8] **By design, MISTI-Israel’s establishment of the Lockheed Martin Seed Fund directly supports the scientific capabilities of the Israeli occupation forces.**

The Israeli occupation forces have brutalized and ethnically cleansed the Palestinian people for the past 75 years since the expulsion of over 700,000 Palestinians during the 1948 Nakba. Despite claims of 'defense', Israeli politicians and officials continue to verbalize the genocidal intent that drives their military operations in the Gaza Strip.

> “This is a struggle between the children of light and the children of darkness, between humanity and the law of the jungle.” —Benjamin Netanyahu, Israeli Prime Minister and MIT alum [^9]

> “I have ordered a complete siege on the Gaza Strip. There will be no electricity, no food, no fuel, everything is closed. We are fighting human animals and we are acting accordingly.” —Yoav Gallant, Israeli Defense Minister [^10]

> “Right now, one goal: Nakba! A Nakba that will overshadow the Nakba of 48. Nakba in Gaza and Nakba to anyone who dares to join!” —Ariel Kallner, a member of the Knesset with the Likud party [^11]

**These dehumanizing messages are proclamations of genocide.**

MISTI-Israel’s creation of the Lockheed Martin Seed Fund makes every member of the MIT community complicit in aiding the science that props up the Israeli occupation. Even the smallest of institutional ties to the forces of occupation and genocide tarnish the labor and integrity of every student and worker at MIT. To expel our complicity, we must actively fight to
end this fund.

At the time of writing, Israel’s occupation forces have bombed hospitals, media towers, residential neighborhoods, and UNRWA refugee schools. The Israeli occupation forces have forcibly displaced over 1 million Gazans, 50% of the population, and have cut water, food, medicine, and fuel off in all neighborhoods. Over 7000 Palestinians have been killed by the Israeli occupation forces, with an estimated 1600 yet to be uncovered under the rubble. [^12][^13][^14] At least 3000 of those murdered by Israel are children.

We, as conscientious scientists and engineers, should not fund, aid, or abet genocide. The existence of such an explicit link between MIT, Israel, and Lockheed Martin is emblematic of the shameful role that universities and other institutions within the US play in upholding this genocide.

**Take a stand against the use of science for genocide. Let us start by ending the MISTI-Israel Lockheed Martin Seed Fund!**

**Initial Signatories:**

MIT Coalition Against Apartheid\
Palestine @ MIT\
MIT Arab Students Organization\
MIT Black Students Union\
MIT Black Students Union Political Action Committee\
MIT Black Graduate Students Association\
MIT Asian American Initiative Executive Board\
MIT Divest\
MIT Reading for Revolution\
MIT Student Worker Alliance\
Boston University Students for Justice in Palestine\
Emerson Students for Justice in Palestine\
Brandeis Students for Justice in Palestine\
Northeastern University Students for Justice in Palestine\
Students for Palestine @ Berklee\
Palestinian Youth Movement - Boston\
Boston South Asian Coalition\
Party for Socialism and Liberation\
Pride at Work Massachusetts\
Wellesley Students for Justice in Palestine

### List of victims of the Israeli aggression:

<object data="Palestinian_Health_Ministry_English.pdf" type="application/pdf" width="100%" style="height:8in">
    <p>Unable to display PDF file. <a href="Palestinian_Health_Ministry_English.pdf">Download</a> instead.</p>
</object>

[^1]: [Defense for Children International Palestine (Oct 13, 2023)](https://www.dci-palestine.org/genocide_in_gaza_nearly_600_palestinian_children_killed_by_israeli_forces)
[^2]: [Center for Constitutional Rights (Oct 18, 2023)](https://ccrjustice.org/israel-s-unfolding-crime-genocide-palestinian-people-us-failure-prevent-and-complicity-genocide)
[^3]: [United Nations (Oct 19, 2023)](https://www.un.org/unispal/document/gaza-un-experts-decry-bombing-of-hospitals-and-schools-as-crimes-against-humanity-call-for-prevention-of-genocide/)
[^4]: [MIT News (Apr 18, 2019)](https://news.mit.edu/2019/lockheed-martin-mit-misti-seed-fund-0418)
[^5]: [MIT-Israel Program Annual Report 2019-2020 (Dec 3, 2020)](https://issuu.com/mistiatmit/docs/misti_ar2020_v9_11302020)
[^6]: [The Jerusalem Post (Apr 14, 2019)](https://www.jpost.com/israel-news/lockheed-martin-mit-launch-fund-to-foster-israeli-research-partnerships-586816)
[^7]: [Global Seed Funds in Israel](https://misti.mit.edu/faculty-funds/available-funds/gsf-israel)
[^8]: [Lockheed Martin in Israel](https://www.lockheedmartin.com/en-il/who-we-are.html)
[^9]: [Ministry of Foreign Affairs Israel (Oct 16, 2023)](https://www.gov.il/en/departments/news/excerpt-from-pm-netanyahu-s-remarks-at-the-opening-of-the-knesset-s-winter-assembly-16-oct-2023)
[^10]: [Times of Israel (Oct 9, 2023)](https://www.timesofisrael.com/liveblog_entry/defense-minister-announces-complete-siege-of-gaza-no-power-food-or-fuel/)
[^11]: [AP News (Oct 14, 2023)](https://apnews.com/article/israel-palestinians-gaza-evacuation-history-nakba-a1bec1ee3477573e80b39b4044a48111)
[^12]: [[Arabic] Palestinian Health Ministry (Oct 26, 2023, stable link)](Palestinian_Health_Ministry_Arabic.pdf)
[^13]: [[English] Palestinian Health Ministry (Oct 26, 2023, stable link)](Palestinian_Health_Ministry_English.pdf) 
[^14]: [Middle East Eye (Oct 26, 2023)](https://www.middleeasteye.net/news/israel-palestine-death-toll-names-killed-released-biden-questions)