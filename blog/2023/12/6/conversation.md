---
layout: blog.pug
title: 'In Conversation: Miko Peled'
subtext: "In continuation of our education series, we are joined by Miko Peled, an Israeli American Pro-Palestinian activist. He grew up in a Zionist household, served in the Israeli Special Forces, but soon abandoned it all and became a strong advocate for Palestinian rights. He will be discussing that journey and more."
date: 2023-12-06T17:00:00-04:00
tags: teachin
---

<iframe class="container" style="aspect-ratio:560/315" src="https://www.youtube-nocookie.com/embed/ABcW7GE3jrw?si=swNIBArH43a_-eB4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In continuation of our education series, we are joined by Miko Peled, an Israeli American Pro-Palestinian activist. He grew up in a Zionist household, served in the Israeli Special Forces, but soon abandoned it all and became a strong advocate for Palestinian rights. He will be discussing that journey and more. 