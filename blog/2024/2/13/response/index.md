---
layout: blog.pug
title: "CAA Suspension Statement"
date: 2024-02-13T21:00:00-04:00
---

Today, the MIT administration suspended the Coalition Against Apartheid for fighting for Palestinian liberation and an end to the genocide. They have further issued sanction letters to 13 student organizers with the Coalition Against Apartheid and the broader Coalition for Palestine, threatening us with *permanent suspension from* MIT.

Yesterday, we called an emergency rally in response to the dire escalation of Israel's genocide of Palestinians. Rafah, a supposed "safe zone," where millions of Palestinans have been forcibly displaced by Israel's genocide, was carpet bombed the night before, threatening millions of refugees with ethnic cleansing by forced removal or by death. It is a deep shame that our institute receives millions of dollars in research funding from the Israeli Ministry of Defense which wages this genocide. We have a moral obligation to fight against these ties.

It is in these moments, when we face the harshest repression, that we know the balance of power is shifting. Their response today reveals that **MIT fears the mass mobilization of our community, who have remained steadfast with Palestine.** Hundreds of our community members, from students and alumni to faculty and staff, have [pledged](https://caa.mit.edu/pledge/) to withhold their labor from companies and research projects on our campus complicit with Israel's genocide. These increasing numbers only show that our community will not back down until MIT cuts all ties to the occupation.

These attacks on our right to protest are not only suppressive but expose the moral failing and desperation of the administration. We must realize, as Martin Luther King, Jr. did, that "a threat to justice anywhere is a threat to justice everywhere," and we must act with urgency when over 35,000 people have been massacred and millions continue to be ethnically cleansed.

Now more than ever, it is crucial to continue fighting for MIT to cut all ties to the genocide. We take leadership from the unbending will of the Palestinian resistance and continue to keep our eyes on Gaza. We call on our community members of conscience to sign our [Scientists Against Apartheid pledge](https://caa.mit.edu/pledge/) now. If you are an organization at MIT, [JOIN THE COALITION IN ACTION](https://forms.gle/nenL9H9q75hmxNin7).

**All eyes on Rafah, all eyes on Gaza, all eyes on Palestine! MIT, cut ties to IOF!**

MIT Coalition Against Apartheid\
Palestine at MIT

Harvard Grads for Palestine\
Northeastern University School of Law Students for Justice in Palestine\
Boston University Students for Justice in Palestine\
University of Massachusetts Boston Students for Justice in Palestine\
Tufts University Students for Justice in Palestine\
Berklee Students for Justice in Palestine\
Party for Socialism and Liberation
